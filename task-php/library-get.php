<!DOCTYPE html>
<html lang="ru">

  <head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta charset="utf-8">
    <title>
      Library card
    </title>
  </head>

  <body>
  
    <form action="library-post.php" method="post">

      <h1><b>Заполните данные </b></h1>

      <label>Имя: <br> 
<input type="text" name="name"> 
</label>


      <br><br>
      <label> Фамилия:<br> 
<input type="text" id="surname-field" name="surname"> 


</label>

      <br>


      <br>
      <label>Пол  <br>
     
<select name="gender" >

<option value="male">Мужчина </option>
<option value="female">Женщина </option>
<option value="secret">Секрет </option>
<option value="alien"> Инопланетный гость </option>
</select></label>

      <br><br>

      <label>Серия и номер паспорта:
<br>
<input type="text" name="document-number"> 
</label>

      <br><br>

      <label>Месяц и год рождения:
<br>
<input type="date" name="date-of-birth"> 
</label>

      <br><br>
      <label> Номер мобильного телефона:
<br>
<input type="text" name="phone-number" placeholder="+7(___)___-__-__"> 
</label>

      <br><br>
      <label> Адрес электронной почты:
<br>
<input type="email" name="email" placeholder="example@mail.com"> 
</label>

      <br>



      <p class="heading">Интересующая литература:</p>
      <div class="book-types">

        <label>
      <input type="checkbox" name="booktype[]" value="1">Художественная литература</label>
        <br>
        <label>
      <input type="checkbox" name="booktype[]" value="2">Бизнес издания</label>
        <br>
        <label>
      <input type="checkbox" name="booktype[]" value="3">Энциклопедии и словари</label>
        <br>
        <label>
      <input type="checkbox" name="booktype[]" value="4">Газеты и журналы </label>
        <br>
        <label>
      <input type="checkbox" name="booktype[]" value="5">Издания для детей </label>
        <br>
        <label>
      <input type="checkbox" name="booktype[]" value="6">Книги на иностранных языках</label>
        <br>
      </div>

      <div class="bordered">

        <p class="heading">Состояли ли вы ранее в нашем клубе читателей? </p>
        <label> 
  <input type="radio" name="readers-club" checked> Да  
</label>
        <label>
  <input type="radio" name="readers-club"> Нет
</label>
      </div>

      <br><br>
      <label> 
      <input type="checkbox" name="agreed" checked> 
      Согласие на обработку персональных данных 
</label>

      <br><br>
      <button class="a01" type="submit">Зарегистрироваться</button>

    </form>
  </body>

</html>

<!DOCTYPE html>
<html lang="ru">

  <head>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta charset="utf-8">
    <title>
      Library card result
    </title>

    <style>
      .empty{
        font-style: italic;
        color: grey;
      }
    </style>

  </head>

  <body>
    <!-- Name block  - it uses inine html style of coding-->
    <?php if (empty($_POST['name'])): ?>
      <p class="empty">Необходимо указать имя!</p>
    <?php else: ?>
      <p>Имя: <?=$_POST['name'];?></p>
    <?php endif?>

    <?php
// Surname block - that one uses classic coding style
if (empty($_POST['surname'])) {
    echo '<p class="empty">Необходимо указать фамилию!</p>';
} else {
    echo '<p>Фамилия: ' . $_POST['surname'] . '</p>';
}
?>

</body>

</html>
